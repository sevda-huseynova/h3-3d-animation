import './App.css';
import React, { useRef, useEffect, useState } from 'react'
import { Suspense } from 'react';
import { useSpring, animated } from '@react-spring/three'
import { Canvas, useLoader, useThree, useFrame, extend } from 'react-three-fiber';
import { useGLTF, OrbitControls, useAnimations } from 'drei'
// import { proxy, useSnapshot } from 'valtio'
// import { proxySet } from 'valtio/utils'


function Sphere({ ...props }) {
  // const [state, setState] = useState(1)
  // const [active, setActive] = useState(false)
  const group = useRef()
  const { nodes, materials, animations } = useGLTF('/shpereH3.glb')
  const [reverse, setReverse] = useState(false)
  const { actions } = useAnimations(animations, group)
  const [num, setNum] = useState(100)
  const myMesh = useRef()
  // const { position } = useSpring({ position: active ? [5, -0.5, 1] : [1, -0.5, 1] })
  const { position } = useSpring({
    to: {
      position: 5
    },
    from: { position: 1 },
    config: { duration: 250 }
  })

  useEffect(() => {
    position.start(10)
    actions.animation_0.play()
  })
  return (

    <group ref={group} {...props} dispose={null}>
      <animated.mesh position={[position, -0.5, 1]} ref={myMesh}>
        <group ref={myMesh}  >
          <mesh
            ref={myMesh}
            scale={2}
            name="Sphere_1"
            geometry={nodes.Sphere_1.geometry}
            material={nodes.Sphere_1.material}
            morphTargetDictionary={nodes.Sphere_1.morphTargetDictionary}
            morphTargetInfluences={nodes.Sphere_1.morphTargetInfluences}
          >
          </mesh>
        </group>
      </animated.mesh>
      <group position={[4, -0.5, 3]}>
        <mesh
          scale={2}
          name="Sphere_2"
          geometry={nodes.Sphere_1.geometry}
          material={nodes.Sphere_1.material}
          morphTargetDictionary={nodes.Sphere_1.morphTargetDictionary}
          morphTargetInfluences={nodes.Sphere_1.morphTargetInfluences}
        >
        </mesh>
      </group>
    </group >
  )
}
useGLTF.preload('/shpereH3.glb')

function App() {
  return (
    <div style={{ background: "black" }}>
      <Canvas shadowMap
        camera={{ position: [1, -10, 1] }}
        style={{ width: "100vw", height: "100vh" }}>
        <Suspense fallback={null}>

          <Sphere />
        </Suspense>
        <directionalLight intensity={1} />
        <spotLight position={[1, 1, 1]} angle={1} />
        <OrbitControls
        // autoRotate={true}
        />
      </Canvas>
    </div>
  );
}

export default App;
